//
// CollectionType.swift
// ODataOfflineTest
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 30/01/19
//

import Foundation

enum CollectionType: String {
    case products = "Products"
    case suppliers = "Suppliers"
    case productCategories = "ProductCategories"
    case purchaseOrderItems = "PurchaseOrderItems"
    case salesOrderItems = "SalesOrderItems"
    case stock = "Stock"
    case salesOrderHeaders = "SalesOrderHeaders"
    case customers = "Customers"
    case purchaseOrderHeaders = "PurchaseOrderHeaders"
    case productTexts = "ProductTexts"
    case none = ""

    static let all = [
        products, suppliers, productCategories, purchaseOrderItems, salesOrderItems, stock, salesOrderHeaders, customers, purchaseOrderHeaders, productTexts,
    ]
}
